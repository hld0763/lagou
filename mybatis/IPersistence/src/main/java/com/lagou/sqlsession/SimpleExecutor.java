package com.lagou.sqlsession;

import com.lagou.config.BoundSql;
import com.lagou.pojo.Configuration;
import com.lagou.pojo.MappedStatement;
import com.lagou.utils.GenericTokenParser;
import com.lagou.utils.ParameterMapping;
import com.lagou.utils.ParameterMappingTokenHandler;
import com.lagou.utils.TokenHandler;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class SimpleExecutor implements Executor {
    @Override
    public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
        //获取连接
        try (Connection connection = configuration.getDataSource().getConnection()) {
            //获取预处理对象
            PreparedStatement preparedStatement = createPreparedStatement(mappedStatement, connection, params);

            //执行sql
            ResultSet resultSet = preparedStatement.executeQuery();

            //封装返回结果
            List<Object> list = new ArrayList<>();
            String resultType = mappedStatement.getResultType();
            Class<?> resultTypeClass = getClassType(resultType);
            while(resultSet.next()) {
                Object o = resultTypeClass.getDeclaredConstructor().newInstance();
                ResultSetMetaData metaData = resultSet.getMetaData();
                for(int i = 1; i<metaData.getColumnCount(); i++) {
                    String columnName = metaData.getColumnName(i);
                    Object value = resultSet.getObject(i);

                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(columnName, resultTypeClass);
                    propertyDescriptor.getWriteMethod().invoke(o, value);
                }
                list.add(o);
            }
            return (List<E>) list;
        }
    }

    @Override
    public void update(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
        //获取连接
        try (Connection connection = configuration.getDataSource().getConnection()) {
            //创建预处理对象
            PreparedStatement preparedStatement = createPreparedStatement(mappedStatement, connection, params);
            //执行sql
            preparedStatement.executeUpdate();
        }
    }

    private PreparedStatement createPreparedStatement(MappedStatement mappedStatement, Connection connection, Object... params) throws Exception {
        //解析sql
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);

        //获取预处理对象
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSqlText());

        //设置参数
        String parameterType = mappedStatement.getParameterType();
        Class<?> parameterTypeClass = getClassType(parameterType);
        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            String content = parameterMapping.getContent();

            Field declaredField = parameterTypeClass.getDeclaredField(content);
            declaredField.setAccessible(true);
            Object o = declaredField.get(params[0]);

            preparedStatement.setObject(i+1, o);
        }

        return preparedStatement;
    }

    private Class<?> getClassType(String parameterType) throws ClassNotFoundException {
        if(parameterType!=null) {
            return Class.forName(parameterType);
        }
        return null;
    }

    /**
     * 完成对sql中的#{}进行处理，替换成?，并存储#{}里面的参数名
     * @param sql
     * @return
     */
    private BoundSql getBoundSql(String sql) {
        ParameterMappingTokenHandler parameterMappingTokenHandler = new ParameterMappingTokenHandler();
        GenericTokenParser genericTokenParser = new GenericTokenParser("#{", "}", parameterMappingTokenHandler);
        //解析过后的sql
        String parseSql = genericTokenParser.parse(sql);
        //解析出来的参数名
        List<ParameterMapping> parameterMappings = parameterMappingTokenHandler.getParameterMappings();
        BoundSql boundSql = new BoundSql(parseSql, parameterMappings);
        return boundSql;
    }
}
