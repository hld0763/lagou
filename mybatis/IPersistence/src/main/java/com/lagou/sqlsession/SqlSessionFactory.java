package com.lagou.sqlsession;

public interface SqlSessionFactory {
    SqlSession openSession();
}
