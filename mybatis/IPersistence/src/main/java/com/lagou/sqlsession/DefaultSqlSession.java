package com.lagou.sqlsession;

import com.lagou.pojo.Configuration;

import java.lang.reflect.*;
import java.util.Collection;
import java.util.List;

public class DefaultSqlSession implements SqlSession {
    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String statementId, Object... params) throws Exception {
        Executor executor = new SimpleExecutor();
        List<E> list = executor.query(configuration, configuration.getMappedStatementMap().get(statementId), params);
        return list;
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        List<Object> list = selectList(statementId, params);
        if(list.size()==1) {
            return (T) list.get(0);
        } else {
            throw new RuntimeException("查询结果不是唯一："+list);
        }
    }

    @Override
    public void update(String statementId, Object... params) throws Exception {
        new SimpleExecutor().update(configuration, configuration.getMappedStatementMap().get(statementId), params);
    }

    @Override
    public <T> T getMapper(Class<?> mapperClass) {
        //使用JDK动态代理来为Dao接口生成代理对象，并返回
        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                String methodName = method.getName();
                String className = method.getDeclaringClass().getName();
                String statementId = className+"."+methodName;

                //通过返回类型是否有泛型来判断调用方法
//                Type genericReturnType = method.getGenericReturnType();
//                if(genericReturnType instanceof ParameterizedType) {
//                    return selectList(statementId, args);
//                } else {
//                    return selectOne(statementId, args);
//                }

                //通过返回类型是否是List来判断调用方法
                Class<?> returnType = method.getReturnType();
                if(List.class.isAssignableFrom(returnType)) {
                    return selectList(statementId, args);
                } else if(returnType.equals(Void.TYPE)) {
                    update(statementId, args);
                    return null;
                } else {
                    return selectOne(statementId, args);
                }
            }
        });
        return (T) proxyInstance;
    }
}
