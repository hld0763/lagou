package com.lagou.sqlsession;

import java.util.List;

public interface SqlSession {

    <E> List<E> selectList(String statementId, Object... params) throws Exception;

    <T> T selectOne(String statementId, Object... params) throws Exception;

    void update(String statementId, Object... params) throws Exception;

    /**
     * 为Dao接口生成代理实现类
     * @param <T>
     * @return
     */
    <T> T getMapper(Class<?> mapperClass);
}
