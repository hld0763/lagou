package com.lagou.io;

import java.io.InputStream;

public class Resource {
    /**
     * 根据配置文件路径，将配置文件加载成字节输入流
     * @param path
     * @return
     */
    public static InputStream getResourceAsStream(String path) {
        return Resource.class.getClassLoader().getResourceAsStream(path);
    }
}
