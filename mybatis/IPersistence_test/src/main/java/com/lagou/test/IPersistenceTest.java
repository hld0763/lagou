package com.lagou.test;

import com.lagou.config.XMLConfigBuilder;
import com.lagou.dao.IUserDao;
import com.lagou.io.Resource;
import com.lagou.pojo.Configuration;
import com.lagou.pojo.User;
import com.lagou.sqlsession.SqlSession;
import com.lagou.sqlsession.SqlSessionFactory;
import com.lagou.sqlsession.SqlSessionFactoryBuilder;
import org.dom4j.DocumentException;
import org.junit.Test;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;

public class IPersistenceTest {

    @Test
    public void test() throws Exception {
        InputStream resourceStream = Resource.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //请完善自定义持久层框架IPersistence，在现有代码基础上添加、修改及删除功能。【需要采用getMapper方式】
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        userDao.findAll().forEach(System.out::println);
        System.out.println("============");
        User user = new User();
        user.setId(4);
        user.setUsername("test");
        userDao.insertUser(user);
        System.out.println(userDao.findByCondition(user));
        user.setUsername("abc");
        userDao.updateUser(user);
        System.out.println(userDao.findByCondition(user));
        userDao.deleteUser(user);
        System.out.println("============");
        userDao.findAll().forEach(System.out::println);
    }
}
