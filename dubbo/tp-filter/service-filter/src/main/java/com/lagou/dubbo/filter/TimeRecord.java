package com.lagou.dubbo.filter;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class TimeRecord {
    private static Map<String, List<Instant[]>> timeMap = new ConcurrentHashMap<>();
    private static Map<String, ReentrantLock> lockMap = new ConcurrentHashMap<>();
    private static volatile boolean printing = false;

    /**
     * 增加方法调用时间记录
     * @param methodName 方法名称
     * @param begin 调用开始时间
     * @param end 调用结束时间
     */
    public static void addTime(String methodName, Instant begin, Instant end) {
        List<Instant[]> list = timeMap.get(methodName);
        if(list==null) {
            //双重检查锁
            synchronized (lockMap) {
                list = timeMap.get(methodName);
                if(list==null) {
                    list = new ArrayList<>(38000);
                    timeMap.put(methodName, list);
                    lockMap.put(methodName, new ReentrantLock());
                }
            }
        }
        if (printing) {
            synchronized (timeMap){}
        }
        ReentrantLock lock = lockMap.get(methodName);
        lock.lock();
        try {
            list.add(new Instant[]{begin, end});
        } finally {
            lock.unlock();
        }
    }

    /**
     * 打印方法调用时间
     */
    public static void printTime() {
        //一分钟前的时间点
        Instant time = Instant.now().minusSeconds(60);
        Map<String, long[]> resultMap;
        synchronized (timeMap) {
            printing = true;
            resultMap = timeMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e->{
                ReentrantLock lock = lockMap.get(e.getKey());
                lock.lock();
                try {
                    //删除开始时间是一分钟前的数据
                    e.getValue().removeIf(t->time.isAfter(t[0]));
                } finally {
                    lock.unlock();
                }
                //统计一分钟内的调用时间，并排好序
                return e.getValue().stream().mapToLong(t-> Duration.between(t[0], t[1]).toMillis()).sorted().toArray();
            }));
            printing = false;
        }
        resultMap.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(e->{
            //循环统计后的结果，并根据需要进行打印
            System.out.println(String.format("%s在1分钟内共执行==>%d次", e.getKey(), e.getValue().length));
            print(e.getKey(), e.getValue(), 90);
            print(e.getKey(), e.getValue(), 99);
        });
        System.out.println("=====================");
    }

    private static void print(String methodName, long[] time, int tp) {
        System.out.println(String.format("%s的TP%d耗时是%d毫秒", methodName, tp, time[time.length*tp/100]));
    }

}
