package com.lagou.dubbo.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

import java.time.Instant;

@Activate(group = {CommonConstants.CONSUMER})
public class TPMonitorFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        Instant time = Instant.now();
        try {
            return invoker.invoke(invocation);
        } finally {
            TimeRecord.addTime(invocation.getMethodName(), time, Instant.now());
        }
    }
}
