package com.lagou.dubbo.service.impl;

import com.lagou.dubbo.service.SleepService;
import org.apache.dubbo.config.annotation.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
public class SleepServiceImpl implements SleepService {

    @Override
    public void methodA() { sleep(); }

    @Override
    public void methodB() {
        sleep();
    }

    @Override
    public void methodC() {
        sleep();
    }

    private void sleep() {
        try {
            //随机休眠0~100毫秒
            Thread.sleep(ThreadLocalRandom.current().nextLong(101));
        } catch (InterruptedException e) {}
    }

}
