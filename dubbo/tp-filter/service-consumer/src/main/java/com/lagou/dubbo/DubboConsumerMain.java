package com.lagou.dubbo;

import com.lagou.dubbo.service.SleepService;
import com.lagou.dubbo.task.MethodATask;
import com.lagou.dubbo.task.MethodBTask;
import com.lagou.dubbo.task.MethodCTask;
import com.lagou.dubbo.task.PrintTask;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DubboConsumerMain {
    public static void main(String[] args) throws IOException, InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        context.start();
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(91);
        scheduledExecutorService.scheduleWithFixedDelay(new PrintTask(),5,5, TimeUnit.SECONDS);
        for (int i = 0; i < 30; i++) {
            scheduledExecutorService.scheduleAtFixedRate(context.getBean(MethodATask.class),0,1,TimeUnit.NANOSECONDS);
            scheduledExecutorService.scheduleAtFixedRate(context.getBean(MethodBTask.class),0,1,TimeUnit.NANOSECONDS);
            scheduledExecutorService.scheduleAtFixedRate(context.getBean(MethodCTask.class),0,1,TimeUnit.NANOSECONDS);
        }

        System.in.read();
        scheduledExecutorService.shutdown();
        context.close();
    }

    @Configuration
    @ComponentScan("com.lagou.dubbo.task")
    @PropertySource("classpath:/dubbo-consumer.properties")
    @EnableDubbo
    static class ConsumerConfiguration {}
}
