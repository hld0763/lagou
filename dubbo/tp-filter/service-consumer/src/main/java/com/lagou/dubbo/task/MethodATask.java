package com.lagou.dubbo.task;

import com.lagou.dubbo.service.SleepService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

@Component
public class MethodATask implements Runnable {

    @Reference
    private SleepService sleepService;

    @Override
    public void run() { sleepService.methodA(); }
}
