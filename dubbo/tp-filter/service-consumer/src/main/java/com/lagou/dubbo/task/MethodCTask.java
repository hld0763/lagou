package com.lagou.dubbo.task;

import com.lagou.dubbo.service.SleepService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MethodCTask implements Runnable {

    @Reference
    private SleepService sleepService;

    @Override
    public void run() {
        sleepService.methodC();
    }
}
