package com.lagou.dubbo.task;

import com.lagou.dubbo.filter.TimeRecord;

public class PrintTask implements Runnable {
    @Override
    public void run() {
        TimeRecord.printTime();
    }
}
