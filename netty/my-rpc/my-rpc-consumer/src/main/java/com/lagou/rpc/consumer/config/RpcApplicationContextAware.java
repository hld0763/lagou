package com.lagou.rpc.consumer.config;

import com.lagou.rpc.consumer.anno.RpcService;
import com.lagou.rpc.consumer.proxy.RpcClientProxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.lang.reflect.Field;

@Component
public class RpcApplicationContextAware implements ApplicationContextAware {
    @Autowired
    private RpcClientProxy rpcClientProxy;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        applicationContext.getBeansWithAnnotation(Controller.class).values().forEach(bean->{
            for (Field field : bean.getClass().getDeclaredFields()) {
                RpcService rpcService = field.getAnnotation(RpcService.class);
                if(rpcService!=null) {
                    field.setAccessible(true);
                    try {
                        field.set(bean, rpcClientProxy.createProxy(field.getType()));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}

