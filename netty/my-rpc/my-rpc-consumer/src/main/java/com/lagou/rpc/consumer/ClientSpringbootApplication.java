package com.lagou.rpc.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientSpringbootApplication.class, args);
    }

}
