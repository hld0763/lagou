package com.lagou.rpc.consumer.controller;


import com.lagou.rpc.api.IUserService;
import com.lagou.rpc.consumer.anno.RpcService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @RpcService
    IUserService userService;

    @RequestMapping("find")
    public String find(String name) {
        return userService.findByName(name);
    }
}