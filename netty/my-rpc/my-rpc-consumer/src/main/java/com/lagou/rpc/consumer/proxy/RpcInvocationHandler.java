package com.lagou.rpc.consumer.proxy;

import com.alibaba.fastjson.JSON;
import com.lagou.rpc.common.RpcRequest;
import com.lagou.rpc.common.RpcResponse;
import com.lagou.rpc.consumer.client.RpcClient;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.UUID;

public class RpcInvocationHandler implements InvocationHandler {
    private int[] port;
    private int portIndex = 0;

    public RpcInvocationHandler(int[] port) {
        this.port = port;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //1.封装request请求对象
        RpcRequest rpcRequest = new RpcRequest();
        rpcRequest.setRequestId(UUID.randomUUID().toString());
        rpcRequest.setClassName(method.getDeclaringClass().getName());
        rpcRequest.setMethodName(method.getName());
        rpcRequest.setParameterTypes(method.getParameterTypes());
        rpcRequest.setParameters(args);
        //2.创建RpcClient对象
        RpcClient rpcClient = new RpcClient("127.0.0.1", getPort());
        try {
            //3.发送消息
            Object responseMsg = rpcClient.send(JSON.toJSONString(rpcRequest));
            RpcResponse rpcResponse = JSON.parseObject(responseMsg.toString(), RpcResponse.class);
            if (rpcResponse.getError() != null) {
                throw new RuntimeException(rpcResponse.getError());
            }
            //4.返回结果
            Object result = rpcResponse.getResult();
//            return JSON.parseObject(result.toString(), method.getReturnType());
            return result;
        } catch (Exception e) {
//            throw e;
            return "RPC失败："+e.getMessage();
        } finally {
            rpcClient.close();
        }
    }

    private int getPort() {
        if(portIndex>=port.length) portIndex = 0;
        return port[portIndex++];
    }
}
