package com.lagou.rpc.consumer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "zk")
@Data
public class ZooKeeperConfig {
    /**
     * zk的server地址，多个server之间使用英文逗号分隔开
     */
    private String server;
}
