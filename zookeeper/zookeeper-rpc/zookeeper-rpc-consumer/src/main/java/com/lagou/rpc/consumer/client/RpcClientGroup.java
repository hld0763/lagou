package com.lagou.rpc.consumer.client;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RpcClientGroup {
    private static Map<String, RpcClient> rpcClientMap = new ConcurrentHashMap<>();
    private static RpcClient[] group = new RpcClient[0];
    private static int index = 0;

    public static RpcClient getRpcClient() {
        if(index>=group.length) index = 0;
        return group[index++];
    }

    public static void removeRpcClient(String host) {
        rpcClientMap.remove(host).close();
        updateGroup();
    }

    public static void putRpcClient(String host) {
        String[] h = host.split(":");
        RpcClient old = rpcClientMap.put(host, new RpcClient(h[0], Integer.parseInt(h[1])));
        updateGroup();
        if(old!=null) old.close();
    }

    private static void updateGroup() {
        group = rpcClientMap.values().toArray(RpcClient[]::new);
    }
}
