package com.lagou.rpc.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.retry.ExponentialBackoffRetry;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ZooKeeperFactory {
    public static final String RPC_SERVER_PATH = "/netty-rpc-server";

    private static Map<String, CuratorFramework> clientMap = new ConcurrentHashMap<>();

    public static CuratorFramework startClient(String zkServer) {
        CuratorFramework client = clientMap.get(zkServer);
        if(client==null || client.getState()!= CuratorFrameworkState.STARTED) {
            client = CuratorFrameworkFactory.builder().connectString(zkServer)
                    .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                    .defaultData(null)
                    .build();
            client.start();
            clientMap.put(zkServer, client);
        }
        return client;
    }

    public static String getChildrenPath(String path) {
        if(path.startsWith(RPC_SERVER_PATH)) return path.substring(RPC_SERVER_PATH.length()+1);
        else return path;
    }

}
