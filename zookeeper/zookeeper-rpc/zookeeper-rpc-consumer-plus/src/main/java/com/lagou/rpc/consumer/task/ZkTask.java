package com.lagou.rpc.consumer.task;

import com.lagou.rpc.consumer.client.RpcClientGroup;
import com.lagou.rpc.zookeeper.ZooKeeperFactory;
import org.apache.curator.framework.CuratorFramework;

import java.util.Map;

public class ZkTask implements Runnable {
    private String zkServer;

    public ZkTask(String zkServer) {
        this.zkServer = zkServer;
    }

    @Override
    public void run() {
        CuratorFramework zkClient = ZooKeeperFactory.startClient(zkServer);
        Map<String, Long> localRequestTimeMap = RpcClientGroup.getLocalRequestTimeMap();
        try {
            zkClient.getChildren().forPath(ZooKeeperFactory.RPC_SERVER_PATH).forEach(path->{
                try {
                    Long time = localRequestTimeMap.get(path);
                    byte[] data = null;
                    if(time!=null) {
                        //时间有效才更新zk上的节点数据，否则清空节点数据
//                        data = ByteBuffer.allocate(8).putLong(time).array();
                        data = time.toString().getBytes();
                    }
                    zkClient.setData().forPath(ZooKeeperFactory.RPC_SERVER_PATH+"/"+path, data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
