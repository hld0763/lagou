package com.lagou.rpc.consumer;

import com.lagou.rpc.consumer.client.RpcClientGroup;
import com.lagou.rpc.consumer.config.ZooKeeperConfig;
import com.lagou.rpc.consumer.task.ZkTask;
import com.lagou.rpc.zookeeper.ZooKeeperFactory;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class ClientSpringbootApplication implements CommandLineRunner {

    @Autowired
    private ZooKeeperConfig zooKeeperConfig;

    public static void main(String[] args) {
        SpringApplication.run(ClientSpringbootApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //定时上报响应时间
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(
                new ZkTask(zooKeeperConfig.getServer()),0,5, TimeUnit.SECONDS);

        //子节点监听器
        new Thread(()->{
            CuratorFramework zkClient = null;
            PathChildrenCache cache = null;
            try {
                //启动zk客户端
                zkClient = ZooKeeperFactory.startClient(zooKeeperConfig.getServer());
                //初始化子节点监听器
                cache = new PathChildrenCache(zkClient, ZooKeeperFactory.RPC_SERVER_PATH, false);
                cache.getListenable().addListener((client, event) -> {
                    switch (event.getType()) {
                        case CHILD_ADDED:
                            RpcClientGroup.putRpcClient(getHost(event));
                            break;
                        case CHILD_REMOVED:
                            RpcClientGroup.removeRpcClient(getHost(event));
                            break;
                    }
                });
                //启动子节点监听器
                cache.start();
            } catch (Exception e) {
                e.printStackTrace();
                if(cache!=null) {
                    try {
                        cache.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
                if(zkClient!=null) {
                    zkClient.close();
                }
            }
        }).start();

    }

    private static String getHost(PathChildrenCacheEvent event) {
        return ZooKeeperFactory.getChildrenPath(event.getData().getPath());
    }
}
