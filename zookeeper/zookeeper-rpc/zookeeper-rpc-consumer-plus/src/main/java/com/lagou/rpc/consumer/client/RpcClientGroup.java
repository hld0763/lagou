package com.lagou.rpc.consumer.client;

import com.lagou.rpc.zookeeper.ZooKeeperFactory;
import org.apache.curator.framework.CuratorFramework;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class RpcClientGroup {
    private static Map<String, RpcClient> rpcClientMap = new ConcurrentHashMap<>();

    public static RpcClient getRpcClient(CuratorFramework zkClient) {
        try {
            long min = Long.MAX_VALUE;
            List<String> minList = new ArrayList<>();
            List<String> pathList = zkClient.getChildren().forPath(ZooKeeperFactory.RPC_SERVER_PATH);
            for (String path : pathList) {
                byte[] data = zkClient.getData().forPath(ZooKeeperFactory.RPC_SERVER_PATH+"/"+path);
                if(data!=null) {
                    long time = Long.parseLong(new String(data));
                    if(time<min) {
                        min = time;
                        minList.clear();
                        minList.add(path);
                    } else if(time==min) {
                        minList.add(path);
                    }
                }
            }
            //如果最小值列表为空就从所有列表中随机获取一个，否则从最小值列表中随机获取一个
            List<String> result = minList.isEmpty()?pathList:minList;
            return rpcClientMap.get(result.get((int)(Math.random()*(result.size()))));
        } catch (Exception e) {
            e.printStackTrace();
            return rpcClientMap.values().stream().findAny().get();
        }
    }

    public static void removeRpcClient(String host) {
        rpcClientMap.remove(host).close();
    }

    public static void putRpcClient(String host) {
        String[] h = host.split(":");
        RpcClient old = rpcClientMap.put(host, new RpcClient(h[0], Integer.parseInt(h[1])));
        if(old!=null) old.close();
    }

    public static Map<String, Long> getLocalRequestTimeMap() {
        return rpcClientMap.entrySet().stream().filter(e->{
            RpcClient client = e.getValue();
            //结束时间在5秒内的数据才记录
            return client.getEndTime()!=null && client.getEndTime().isAfter(Instant.now().plusSeconds(-5));
        }).collect(Collectors.toMap(Map.Entry::getKey,e->{
            RpcClient client = e.getValue();
            //计算rpc调用的开始时间到结束时间的毫秒数
            return Duration.between(client.getBeginTime(), client.getEndTime()).toMillis();
        }));
    }

}
