package com.lagou.edu.proxy.impl;

import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.annotation.Component;
import com.lagou.edu.proxy.ProxyFactory;
import com.lagou.edu.transaction.TransactionManager;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 代理对象工厂：生成代理对象的
 */
@Component("proxyFactory")
public class ProxyFactoryImpl implements ProxyFactory {
    
    @Autowired
    private TransactionManager transactionManager;

    /**
     * Jdk动态代理
     * @param obj  委托对象
     * @return   代理对象
     */
    @Override
    public Object getJdkProxy(Object obj) {
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(),(proxy, method, args)->doProxy(obj, method, args));
    }

    /**
     * 使用cglib动态代理生成代理对象
     * @param obj 委托对象
     * @return
     */
    @Override
    public Object getCglibProxy(Object obj) {
        return  Enhancer.create(obj.getClass(), (MethodInterceptor) (o, method, objects, methodProxy) -> doProxy(obj, method, objects));
    }

    private Object doProxy(Object obj, Method method, Object[] args) throws Exception {
        Object result = null;
        try{
            // 开启事务(关闭事务的自动提交)
            transactionManager.beginTransaction();

            result = method.invoke(obj, args);

            // 提交事务
            transactionManager.commit();
        }catch (Exception e) {
//            e.printStackTrace();
            // 回滚事务
            transactionManager.rollback();

            // 抛出异常便于上层servlet捕获
            throw e;
        }
        return result;
    }
}
