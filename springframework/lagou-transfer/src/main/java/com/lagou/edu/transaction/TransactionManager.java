package com.lagou.edu.transaction;

import java.sql.SQLException;

public interface TransactionManager {
    /**
     * 开启事务
     * @throws SQLException
     */
    void beginTransaction() throws SQLException;

    /**
     * 提交事务
     * @throws SQLException
     */
    void commit() throws SQLException;


    /**
     * 回滚事务
     * @throws SQLException
     */
    void rollback() throws SQLException;
}
