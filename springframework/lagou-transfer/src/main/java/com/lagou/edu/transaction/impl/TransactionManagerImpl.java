package com.lagou.edu.transaction.impl;

import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.annotation.Component;
import com.lagou.edu.transaction.TransactionManager;
import com.lagou.edu.utils.ConnectionUtils;

import java.sql.SQLException;

@Component("transactionManager")
public class TransactionManagerImpl implements TransactionManager {

    @Autowired
    private ConnectionUtils connectionUtils;

    /**
     * 开启事务
     * @throws SQLException
     */
    @Override
    public void beginTransaction() throws SQLException {
        connectionUtils.getCurrentThreadConn().setAutoCommit(false);
    }

    /**
     * 提交事务
     * @throws SQLException
     */
    @Override
    public void commit() throws SQLException {
        connectionUtils.getCurrentThreadConn().commit();
    }


    /**
     * 回滚事务
     * @throws SQLException
     */
    @Override
    public void rollback() throws SQLException {
        connectionUtils.getCurrentThreadConn().rollback();
    }


}
