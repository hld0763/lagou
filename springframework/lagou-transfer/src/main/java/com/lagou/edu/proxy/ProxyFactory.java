package com.lagou.edu.proxy;

public interface ProxyFactory {

    /**
     * Jdk动态代理
     * @param obj  委托对象
     * @return   代理对象
     */
    Object getJdkProxy(Object obj);

    /**
     * 使用cglib动态代理生成代理对象
     * @param obj 委托对象
     * @return
     */
    Object getCglibProxy(Object obj);

}
