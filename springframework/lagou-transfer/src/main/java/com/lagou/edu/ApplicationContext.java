package com.lagou.edu;

import com.lagou.edu.annotation.*;
import com.lagou.edu.proxy.ProxyFactory;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Bean容器
 */
public class ApplicationContext {
    // 自定义@Service、@Autowired、@Transactional注解类，完成基于注解的IOC容器（Bean对象创建及依赖注入维护）和声明式事务控制，
    // 写到转账工程中，并且可以实现转账成功和转账异常时事务回滚

    /**
     * 收集扫描到有注解的类
     */
    private Set<Class> annotationClass;

    /**
     * 初始化时缓存Bean
     */
    private Map<String, Object> initBeanMap;

    /**
     * 存放初始化完成的Bean
     */
    private Map<String, Object> beanMap;

    /**
     * 缓存代理工厂Bean
     */
    private ProxyFactory proxyFactory;

    /**
     * 初始化容器
     * @throws Exception
     */
    public void init() throws Exception {
        annotationClass = new HashSet<>();
        scan(ApplicationContext.class.getPackageName());
        initAnnotationClass();
        clearInitCache();
        //打印容器的所有Bean
        beanMap.entrySet().forEach(entry -> System.out.println(entry.getKey()+":"+entry.getValue()));
    }

    /**
     * 获取Bean
     * @param beanName
     * @return
     */
    public Object getBean(String beanName) {
        return beanMap.get(beanName);
    }

    /**
     * 初始化有注解的类
     */
    private void initAnnotationClass() throws Exception {
        initBeanMap = new HashMap<>(annotationClass.size());
        for (Class clazz : annotationClass) {
            //实例化Bean
            Object instance = clazz.getConstructor().newInstance();
            //获取beanName
            String beanName;
            Component component = (Component)clazz.getDeclaredAnnotation(Component.class);
            if(component!=null) {
                beanName = component.value();
            } else {
                Service service = (Service)clazz.getDeclaredAnnotation(Service.class);
                beanName = service.value();
            }
            //加到初始化Bean缓存里
            initBeanMap.put(beanName, instance);
        }
        //处理自动加载属性
        for (Object bean : initBeanMap.values()) {
            autowired(bean);
            //判断是不是ProxyFactory实现类
            if(bean instanceof ProxyFactory) {
                proxyFactory = (ProxyFactory) bean;
            }
        }
        beanMap = new HashMap<>(initBeanMap.size());
        for (Map.Entry<String, Object> entry : initBeanMap.entrySet()) {
            //处理事务
            String beanName = entry.getKey();
            Object bean = entry.getValue();
            bean = transactional(beanName, bean);
            //保存初始化完成的Bean
            beanMap.put(beanName, bean);
        }
    }

    /**
     * 清除初始化过程中的缓存
     */
    private void clearInitCache() {
        annotationClass.clear();
        initBeanMap.clear();
        proxyFactory = null;
    }

    /**
     * 检查自动加载的属性并处理
     */
    private void autowired(Object bean) throws Exception {
        for (Field field : bean.getClass().getDeclaredFields()) {
            if(field.isAnnotationPresent(Autowired.class)) {
                field.setAccessible(true);
                field.set(bean, initBeanMap.get(field.getName()));
            }
        }
    }

    /**
     * 检查Bean是否启用事务，启用的话要用代理类包装
     */
    private Object transactional(String beanName, Object bean) {
        Transactional annotation = bean.getClass().getAnnotation(Transactional.class);
        if(annotation!=null) {
            //判断Bean有没有实现接口，有就用jdk代理，没有就用cglib代理
            if(bean.getClass().getInterfaces().length>0) {
                return proxyFactory.getJdkProxy(bean);
            } else {
                return proxyFactory.getCglibProxy(bean);
            }
        }
        return bean;
    }

    /**
     * 递归扫描包
     * @param packageName
     * @throws Exception
     */
    private void scan(String packageName) throws Exception {
        for (File file : new File(getClass().getClassLoader().getResource(packageName.replace('.', '/')).toURI()).listFiles()) {
            String fileName = file.getName();
            if(file.isDirectory()) {
                scan(packageName+"."+ fileName);
            } else if(fileName.endsWith(".class")) {
                checkAnnotationClass(packageName+"."+ fileName.substring(0, fileName.length()-6));
            }
        }
    }

    /**
     * 判断类是否有注解Service或TransactionalManager，有才
     * @param className
     * @throws Exception
     */
    private void checkAnnotationClass(String className) {
        try {
            Class<?> clazz = Class.forName(className, false, getClass().getClassLoader());
            if(clazz.isAnnotationPresent(Component.class) || clazz.isAnnotationPresent(Service.class)) {
                annotationClass.add(clazz);
            }
        } catch (ClassNotFoundException|NoClassDefFoundError e) {
            //无法加载的类不处理
        }
    }

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = new ApplicationContext();
        applicationContext.init();
        System.out.println(applicationContext.getBean("transferService"));
    }
}
