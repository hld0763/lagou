create table account
(
	name varchar(255) null comment '用户名',
	money int null comment '账户金额',
	cardNo varchar(255) not null comment '银行卡号',
	constraint account_pk
		primary key (cardNo)
);

INSERT INTO `account` (`name`, `money`, `cardNo`) VALUES ('李雷', 10000, '13800');

INSERT INTO `account` (`name`, `money`, `cardNo`) VALUES ('韩梅梅', 10000, '10086');