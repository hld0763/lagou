<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>SSS测试</title>
</head>
<body>
<p>hello ${sessionScope.user}</p>
<form action="save" method="post">
    <fieldset>
        <legend>新增记录</legend>
        <input name="name" placeholder="名称"/>
        <input name="address" placeholder="地址"/>
        <input name="phone" placeholder="手机号"/>
        <button type="submit">保存</button>
    </fieldset>
</form>
<form onsubmit="return false;">
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>名称</th>
        <th>地址</th>
        <th>手机号</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${list}" var="info">
        <tr id="tr${info.id}">
            <td>${info.id}<input type="hidden" id="id${info.id}" name="id" value="${info.id}"/></td>
            <td><input id="name${info.id}" name="name" value="${info.name}"/></td>
            <td><input id="address${info.id}" name="address" value="${info.address}"/></td>
            <td><input id="phone${info.id}" name="phone" value="${info.phone}"/></td>
            <td>
                <a href="javascript:;" onclick="updateResume('${info.id}')">修改</a>
                <a href="javascript:;" onclick="deleteResume('${info.id}')">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</form>
<script type="text/javascript" src="jquery.min.js"></script>
<script>
    function updateResume(id) {
        $.post("update", {id:$("#id"+id).val(),name:$("#name"+id).val(),address:$("#address"+id).val(),phone:$("#phone"+id).val()}, function (data) {
            alert("修改成功");
        });
    }

    function deleteResume(id) {
        $.post("delete", {id:id}, function (data) {
            $("#tr"+id).remove();
            alert("删除成功");
        });
    }
</script>
</body>
</html>
