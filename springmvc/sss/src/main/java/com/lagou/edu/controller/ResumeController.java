package com.lagou.edu.controller;

import com.lagou.edu.pojo.Resume;
import com.lagou.edu.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ResumeController {

    @Autowired
    private ResumeService resumeService;

    /**
     * 首页显示列表数据
     * @param model
     * @return
     */
    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("list", resumeService.queryResumeList());
        return "index";
    }

    /**
     * 保存数据
     * @param resume
     * @return
     */
    @PostMapping("/save")
    public String save(Resume resume) {
        resumeService.saveOrUpdateResume(resume);
        return "redirect:/";
    }

    /**
     * 修改数据
     * @param resume
     */
    @PostMapping("/update")
    @ResponseBody
    public void update(Resume resume) {
        resumeService.saveOrUpdateResume(resume);
    }

    /**
     * 删除数据
     * @param id
     */
    @RequestMapping("/delete")
    @ResponseBody
    public void delete(Long id) {
        resumeService.deleteResume(id);
    }

}
