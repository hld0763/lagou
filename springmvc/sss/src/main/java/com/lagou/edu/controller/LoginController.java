package com.lagou.edu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {

    /**
     * 去到登录页面
     */
    @GetMapping("/login")
    public void login() {
    }

    /**
     * 进行登录
     * @param request
     * @param model
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/login")
    public String login(HttpServletRequest request, Model model, String username, String password) {
        //用户名和密码固定为admin/admin
        if("admin".equals(username) && username.equals(password)) {
            request.getSession().setAttribute("user", username);
            return "redirect:/";
        }
        //来到这说明登录失败
        model.addAttribute("error","用户名密码错误");
        return "login";
    }

}
