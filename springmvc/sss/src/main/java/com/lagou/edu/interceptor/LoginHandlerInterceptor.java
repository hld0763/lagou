package com.lagou.edu.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 登录验证的拦截器
 */
public class LoginHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断是否有登录信息
        HttpSession session = request.getSession(false);
        if(session!=null && session.getAttribute("user")!=null) {
            //有登录信息则验证通过，可以访问
            return true;
        }
        //来到这说明验证不通过，去到登录页面
        response.sendRedirect(request.getContextPath()+"/login");
        return false;
    }
}
