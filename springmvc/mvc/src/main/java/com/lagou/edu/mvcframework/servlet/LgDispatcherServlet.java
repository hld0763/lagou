package com.lagou.edu.mvcframework.servlet;

import com.lagou.edu.mvcframework.annotations.*;
import com.lagou.edu.mvcframework.pojo.Handler;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LgDispatcherServlet extends HttpServlet {
    //手写MVC框架基础上增加如下功能
    //
    //1）定义注解@Security（有value属性，接收String数组），该注解用于添加在Controller类或者Handler方法上，表明哪些用户拥有访问该Handler方法的权限（注解配置用户名）
    //
    //2）访问Handler时，用户名直接以参数名username紧跟在请求的url后面即可，比如http://localhost:8080/demo/handle01?username=zhangsan
    //
    //3）程序要进行验证，有访问权限则放行，没有访问权限在页面上输出

    private Properties properties = new Properties();

    /**
     * 缓存扫描到的类的全限定类名
     */
    private List<String> classNames = new ArrayList<>();

    /**
     * 存储url和method之间的映射关系
     */
    private List<Handler> handleMapping = new ArrayList<>();

    /**
     * ioc容器
     */
    private Map<String, Object> ioc = new HashMap<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 根据uri获取能够处理当前请求的handler
        Handler handler = getHandler(req);
        if(handler==null) {
            resp.getWriter().write("404 not found");
            return;
        }
        // 判断是否要验证权限
        List<String> securityList = handler.getSecurityList();
        if(securityList!=null && !securityList.isEmpty()) {
            // 权限列表不为空表示需要验证权限
            String username = req.getParameter("username");
            if(!securityList.contains(username)) {
                // 没有权限直接返回
                resp.getWriter().write("403 Forbidden");
                return;
            }
        }

        // 参数绑定
        // 获取所有参数类型数组，这个数组的长度就是我们最后要传入的args数组的长度
        Class<?>[] parameterTypes = handler.getMethod().getParameterTypes();

        // 根据上述数组长度创建一个新的数组（参数数组，是要传入反射调用的）
        Object[] paraValues = new Object[parameterTypes.length];

        // 以下就是为了向参数数组中塞值，而且还得保证参数的顺序和方法中形参顺序一致
        Map<String, String[]> parameterMap = req.getParameterMap();
        for (Map.Entry<String, String[]> param : parameterMap.entrySet()) {
            // 如果参数和方法中的参数匹配上了，填充参数
            if(!handler.getParamIndexMapping().containsKey(param.getKey())) continue;
//            String value = StringUtils.join(param.getValue(), ",");
            String value = Arrays.stream(param.getValue()).collect(Collectors.joining(","));
            // 方法形参确实有该参数，找到它的索引位置，对应的把参数值放入paraValue
            paraValues[handler.getParamIndexMapping().get(param.getKey())] = value;
        }

        int requestIndex = handler.getParamIndexMapping().get(HttpServletRequest.class.getSimpleName());
        paraValues[requestIndex] = req;

        int responseIndex = handler.getParamIndexMapping().get(HttpServletResponse.class.getSimpleName());
        paraValues[responseIndex] = resp;

        // 最终调用handler的method属性
        try {
            Object result = handler.getMethod().invoke(handler.getController(), paraValues);
            if(result!=null && result instanceof String) {
                resp.getWriter().write((String)result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Handler getHandler(HttpServletRequest req) {
        if(handleMapping.isEmpty()) return null;
        String url = req.getRequestURI();
        for (Handler handler : handleMapping) {
            Matcher matcher = handler.getPattern().matcher(url);
            if(matcher.matches()) {
                return handler;
            }
        }
        return null;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            // 1.加载配置文件
            String contextConfigLocation = config.getInitParameter("contextConfigLocation");
            doLoadConfig(contextConfigLocation);

            // 2.扫描相关的类、注解
            doScan(properties.getProperty("scanPackage"));

            // 3.初始化Bean对象
            doInstance();

            // 4.实现依赖注入
            doAutoWired();

            // 5.构造HandlerMapping，建立请求映射
            initHandlerMapping();

            System.out.println("lagou mvc 初始化完成...");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }

    /**
     * 构造HandlerMapping
     */
    private void initHandlerMapping() {
        if(ioc.isEmpty()) return;
        ioc.entrySet().forEach(entry -> {
            // 获取ioc中当前遍历的对象的class类型
            Class<?> aClass = entry.getValue().getClass();
            if(aClass.isAnnotationPresent(LagouController.class)) {
                String baseUrl = "";
                // 处理类上的LagouRequestMapping注解
                if(aClass.isAnnotationPresent(LagouRequestMapping.class)) {
                    LagouRequestMapping annotation = aClass.getAnnotation(LagouRequestMapping.class);
                    baseUrl = annotation.value();
                }

                // 用户访问权限，默认是空列表，表示不限制访问权限
                List<String> defaultUser = Collections.emptyList();
                //处理类上的LagouSecurity注解，在类上有LagouSecurity注解表示该类的所有handle方法默认只有指定用户能访问
                if(aClass.isAnnotationPresent(LagouSecurity.class)) {
                    LagouSecurity annotation = aClass.getAnnotation(LagouSecurity.class);
                    defaultUser = Arrays.asList(annotation.value());
                }

                // 获取方法
                for (Method method : aClass.getDeclaredMethods()) {
                    // 如果标识，就处理
                    if(method.isAnnotationPresent(LagouRequestMapping.class)) {
                        LagouRequestMapping annotation = method.getAnnotation(LagouRequestMapping.class);
                        String url = baseUrl+annotation.value();
                        // 把method所有信息及url封装为一个handler
                        Handler handler = new Handler(entry.getValue(), method, Pattern.compile(url));
                        // 计算方法的参数位置信息
                        Parameter[] parameters = method.getParameters();
                        for (int i = 0; i < parameters.length; i++) {
                            Parameter parameter = parameters[i];
                            if(parameter.getType()==HttpServletRequest.class ||
                                    parameter.getType()==HttpServletResponse.class) {
                                // 如果是request和response对象，那么参数名称写HttpServletRequest和HttpServletResponse
                                handler.getParamIndexMapping().put(parameter.getType().getSimpleName(), i);
                            } else {
                                handler.getParamIndexMapping().put(parameter.getName(), i);
                            }
                        }

                        // 处理方法上的LagouSecurity注解，在方法上有LagouSecurity注解表示该方法只有指定用户能访问，会覆盖类的LagouSecurity注解配置
                        if(method.isAnnotationPresent(LagouSecurity.class)) {
                            handler.setSecurityList(Arrays.asList(method.getAnnotation(LagouSecurity.class).value()));
                        } else {
                            // 如果方法上没有LagouSecurity注解，则使用类的LagouSecurity注解
                            handler.setSecurityList(defaultUser);
                        }

                        // 将映射信息缓存起来
                        handleMapping.add(handler);
                    }
                }
            }
        });
    }

    /**
     * 实现依赖注入
     */
    private void doAutoWired() throws Exception {
        if(ioc.isEmpty()) return;
        // 遍历ioc中所有对象，查看对象中的字段，是否有@LagouAutowired注解，如果有需要维护依赖注入关系
        for (Map.Entry<String, Object> entry : ioc.entrySet()) {
            // 获取bean对象中的字段信息
            Field[] declaredFields = entry.getValue().getClass().getDeclaredFields();
            // 遍历判断处理
            for (Field declaredField : declaredFields) {
                if(declaredField.isAnnotationPresent(LagouAutowired.class)) {
                    // 有注解
                    LagouAutowired annotation = declaredField.getAnnotation(LagouAutowired.class);
                    // 需要注入的bean的id
                    String beanName = annotation.value();
                    if(beanName.trim().length()==0) {
                        // 没有配置具体的bean id，那就需要根据当前字段类型注入（接口注入）
                        beanName = declaredField.getType().getName();
                    }

                    //开启赋值
                    declaredField.setAccessible(true);
                    declaredField.set(entry.getValue(), ioc.get(beanName));
                }
            }
        }
    }

    /**
     * ioc容器，基于classNames缓存类的全限定类名，以及反射技术，完成对象创建和管理
     */
    private void doInstance() throws Exception {
        if(classNames.isEmpty()) return;

        for (String className : classNames) {
            // 反射
            Class<?> aClass = Class.forName(className);
            // 区分controller，区分service
            if(aClass.isAnnotationPresent(LagouController.class)) {
                // 将类的首字母小写保存到作为id，保存到ioc容器里
                ioc.put(lowerFirst(aClass.getSimpleName()), aClass.getConstructor().newInstance());
            } else if(aClass.isAnnotationPresent(LagouService.class)) {
                LagouService annotation = aClass.getAnnotation(LagouService.class);
                // 获取注解的值
                String beanName = annotation.value();
                // 如果指定了id，就以指定的id为准
                // 如果没有指定，就以类首字母小写
                if("".equals(beanName.trim())) {
                    beanName = lowerFirst(aClass.getSimpleName());
                }
                Object o = aClass.getConstructor().newInstance();
                ioc.put(beanName, o);

                // service层往往是有接口的，面向接口开发，此时再以接口名为id，放入一份对象到ioc中，便于后期根据类型注入
                for (Class<?> anInterface : aClass.getInterfaces()) {
                    // 以接口的全限定类名作为id放入
                    ioc.put(anInterface.getName(), o);
                }
            }
        }

    }

    private String lowerFirst(String str) {
        char[] chars = str.toCharArray();
        if(chars[0]>='A' && chars[0]<='A') {
            chars[0]+=32;
        }
        return String.valueOf(chars);
    }

    /**
     * 扫描路径
     */
    private void doScan(String scanPackage) {
        String scanPackagePath = Thread.currentThread().getContextClassLoader().getResource("").getPath() + scanPackage.replace('.', '/');
        File dir = new File(scanPackagePath);
        for (File file : dir.listFiles()) {
            String fileName = file.getName();
            if(file.isDirectory()) {
                // 递归扫描子文件夹
                doScan(scanPackage+"."+ fileName);
            } else if(fileName.endsWith(".class")) {
                // 收集扫描到的class
                classNames.add(scanPackage+"."+fileName.substring(0,fileName.length()-6));
            }
        }
    }

    /**
     * 加载配置文件
     * @param contextConfigLocation
     */
    private void doLoadConfig(String contextConfigLocation) {
        try (InputStream in = getClass().getClassLoader().getResourceAsStream(contextConfigLocation)) {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
