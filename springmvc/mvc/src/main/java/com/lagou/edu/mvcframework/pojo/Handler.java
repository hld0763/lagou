package com.lagou.edu.mvcframework.pojo;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class Handler {

    private Object controller;

    private Method method;

    private Pattern pattern;

    /**
     * 参数顺序，是为了进行参数绑定，key是参数名，value代表第几个参数
     */
    private Map<String, Integer> paramIndexMapping;

    /**
     * 可访问的用户列表
     */
    private List<String> securityList;

    public Handler() {
    }

    public Handler(Object controller, Method method, Pattern pattern) {
        this.controller = controller;
        this.method = method;
        this.pattern = pattern;
        this.paramIndexMapping = new HashMap<>();
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public Map<String, Integer> getParamIndexMapping() {
        return paramIndexMapping;
    }

    public void setParamIndexMapping(Map<String, Integer> paramIndexMapping) {
        this.paramIndexMapping = paramIndexMapping;
    }

    public List<String> getSecurityList() {
        return securityList;
    }

    public void setSecurityList(List<String> securityList) {
        this.securityList = securityList;
    }
}
