package com.lagou.demo.controller;

import com.lagou.demo.service.IDemoService;
import com.lagou.edu.mvcframework.annotations.LagouAutowired;
import com.lagou.edu.mvcframework.annotations.LagouController;
import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
import com.lagou.edu.mvcframework.annotations.LagouSecurity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@LagouController
@LagouRequestMapping("/demo")
@LagouSecurity({"a","c"})
public class DemoController {

    @LagouAutowired
    private IDemoService demoService;

    /**
     * URL: /demo/query?name=hello
     * @param request
     * @param response
     * @param name
     * @return
     */
    @LagouRequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String name) {
        return demoService.get(name);
    }

    /**
     *
     * @param request
     * @param response
     * @param username
     * @return
     */
    @LagouRequestMapping("/handle01")
    @LagouSecurity("b")
    public String handle01(HttpServletRequest request, HttpServletResponse response, String username) {
        return "Hello "+username.toUpperCase();
    }

    /**
     *
     * @param request
     * @param response
     * @param username
     * @return
     */
    @LagouRequestMapping("/handle02")
    public String handle02(HttpServletRequest request, HttpServletResponse response, String username) {
        return "Hello "+username.toUpperCase();
    }
}
