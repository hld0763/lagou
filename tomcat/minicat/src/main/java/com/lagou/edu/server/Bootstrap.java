package com.lagou.edu.server;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Minicat的主类
 */
public class Bootstrap {

    /**
     * 默认端口号
     */
    private int port = 8080;

    private Map<String, HttpServlet> servletMap = new HashMap();
    private ServletMapper servletMapper = new ServletMapper();

    /**
     * Minicat初始化时的一些操作
     */
    public void start() throws IOException {
        //加载配置
//        loadServlet();
        loadServer();

        //定义线程池
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 10, 5, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10));

        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("=======>>>>Minicat start on port:"+port);

        while (true) {
            Socket socket = serverSocket.accept();

//            threadPoolExecutor.execute(new RequestProcesser(socket, servletMap));
            threadPoolExecutor.execute(new RequestProcesser(socket, servletMapper));

        }
    }

    /**
     * Minicat的启动入口
     * @param args
     */
    public static void main(String[] args) {
        //在原有Minicat的基础上进一步扩展，模拟出webapps部署效果
        Bootstrap bootstrap = new Bootstrap();
        try {
            //启动Minicat
            bootstrap.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 加载解析server.xml，初始化应用
     */
    private void loadServer() {
        try(InputStream server = this.getClass().getClassLoader().getResourceAsStream("server.xml")) {
            SAXReader saxReader = new SAXReader();
            Document document = saxReader.read(server);
            Node service = document.selectSingleNode("/Server/Service");
            //解析监听的端口
            Element connector = (Element) service.selectSingleNode("Connector");
            this.port = Integer.parseInt(connector.attributeValue("port"));
            //解析应用
            Element engine = (Element) service.selectSingleNode("Engine");
            List<Element> hosts = engine.selectNodes("Host");
            for (Element host : hosts) {
                String name = host.attributeValue("name");
                String appBase = host.attributeValue("appBase");
                servletMapper.addMapper(name, appBase);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加载解析web.xml，初始化servlet
     */
    private void loadServlet() {
        InputStream web = this.getClass().getClassLoader().getResourceAsStream("web.xml");
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(web);
            Element rootElement = document.getRootElement();
            List<Element> selectNodes = rootElement.selectNodes("//servlet");
            for (Element element : selectNodes) {
                //<servlet-name>lagou</servlet-name>
                String servletName = element.selectSingleNode("servlet-name").getStringValue();
                //<servlet-class>com.lagou.edu.server.LagouServlet</servlet-class>
                String servletClass = element.selectSingleNode("servlet-class").getStringValue();
                //<url-pattern>/lagou</url-pattern>
                String urlPattern = rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']").selectSingleNode("url-pattern").getStringValue();

                servletMap.put(urlPattern, (HttpServlet) Class.forName(servletName).getConstructor().newInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
