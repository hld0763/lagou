package com.lagou.edu.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 静态资源处理类
 */
public class StaticResourceUtil {
    /**
     * 获取静态资源的绝对路径
     * @param path
     * @return
     */
    public static String getAbsolutePath(String path) {
        String absolutePath = StaticResourceUtil.class.getResource("/").getPath();
        return absolutePath.replaceAll("\\\\", "/")+path;
    }

    /**
     * 读取静态资源文件流，通过输出流输出
     * @param inputStream
     * @param outputStream
     */
    public static void outputStaticResource(InputStream inputStream, OutputStream outputStream) throws IOException {
        int resourceSize = inputStream.available();
        //输出响应头
        outputStream.write(HttpProtocolUtil.getHttpHeader200(resourceSize).getBytes());
        //输出响应体
        byte[] bytes = new byte[1024];
        int count;
        while((count = inputStream.read(bytes))>=0) {
            if(count>0) {
                outputStream.write(bytes, 0, count);
            }
        }
    }
}
