package com.lagou.edu.server;

import java.io.*;

/**
 * 把请求封装为Request对象
 */
public class Request {
    private String method;
    private String url;
    private String appPath;
    private String reqPath;
    private String host;
    private InputStream inputStream;

    /**
     * 通过输入流构造Request对象
     * @param inputStream
     */
    public Request(InputStream inputStream) throws IOException {
        this.inputStream = inputStream;

        //从输入流获取数据
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String firstLine = in.readLine();
        System.out.println(firstLine); //GET / HTTP/1.1
        String[] split = firstLine.split(" ");
        this.method = split[0];
        this.url = split[1];

        //解析地址 /demo1/lagou
        String path = this.url;
        if(path.startsWith("/")) path = path.substring(1);
        int i = path.indexOf('/');
        if(i>0) {
            //将demo1解析出来
            this.appPath = path.substring(0, i);
            //将/lagou解析出来
            this.reqPath = path.substring(i);
        } else {
            this.appPath = path;
            this.reqPath = "/";
        }

        //从输入流获取Host数据
        String header;
        while ((header = in.readLine())!=null) {
            if(header.startsWith("Host: ")) {
                this.host = header.split(" ")[1];
                //如果有端口就去掉
                int ii = this.host.indexOf(':');
                if(ii>=0) {
                    this.host = this.host.substring(0, ii);
                }
                break;
            }
        }

        System.out.println("=====>>>method:"+method);
        System.out.println("=====>>>url:"+url);
        System.out.println("=====>>>appPath:"+appPath);
        System.out.println("=====>>>reqPath:"+reqPath);
        System.out.println("=====>>>host:"+host);
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAppPath() {
        return appPath;
    }

    public void setAppPath(String appPath) {
        this.appPath = appPath;
    }

    public String getReqPath() {
        return reqPath;
    }

    public void setReqPath(String reqPath) {
        this.reqPath = reqPath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }
}
