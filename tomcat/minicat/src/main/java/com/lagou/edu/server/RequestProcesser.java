package com.lagou.edu.server;

import java.net.Socket;
import java.util.Map;

public class RequestProcesser extends Thread {
    private Socket socket;
    private Map<String, HttpServlet> servletMap;
    private ServletMapper servletMapper;
    public RequestProcesser(Socket socket, Map<String, HttpServlet> servletMap) {
        this.socket = socket;
        this.servletMap = servletMap;
    }

    public RequestProcesser(Socket socket, ServletMapper servletMapper) {
        this.socket = socket;
        this.servletMapper = servletMapper;
    }

    @Override
    public void run() {
        try {
            Request request = new Request(socket.getInputStream());
            Response response = new Response(socket.getOutputStream());

//            HttpServlet servlet = servletMap.get(request.getUrl());
            HttpServlet servlet = servletMapper.getServlet(request.getHost(), request.getAppPath(), request.getReqPath());
            if(servlet==null) {
                //处理静态资源
//                response.outputHtml(request.getUrl());
                response.outputHtml(servletMapper.getResource(request.getHost(), request.getAppPath(), request.getReqPath()));
            } else {
                //处理动态servlet
                try {
                    servlet.service(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
