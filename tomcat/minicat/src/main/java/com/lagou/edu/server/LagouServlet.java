package com.lagou.edu.server;

import java.io.IOException;

public class LagouServlet extends HttpServlet {
    @Override
    public void doGet(Request request, Response response) {
        String content = "<h1>LagouServlet GET demo3</h1>";
        try {
            response.output(HttpProtocolUtil.getHttpHeader200(content.length())+content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(Request request, Response response) {
        String content = "<h1>LagouServlet POST demo3</h1>";
        try {
            response.output(HttpProtocolUtil.getHttpHeader200(content.length())+content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws Exception {

    }

    @Override
    public void destroy() throws Exception {

    }
}
