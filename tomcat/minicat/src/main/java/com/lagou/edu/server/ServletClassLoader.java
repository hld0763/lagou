package com.lagou.edu.server;

import java.io.File;
import java.io.FileInputStream;

/**
 * 自定义类加载器，绕过双亲委派机制
 */
public class ServletClassLoader extends ClassLoader {
    private File rootDir;

    public ServletClassLoader(File rootDir) {
        this.rootDir = rootDir;
    }

    /**
     * 加载servlet-class
     * @param name servlet-class
     * @return
     * @throws ClassNotFoundException
     */
    public Class<?> loadServletClass(String name) throws ClassNotFoundException {
        byte[] b;
        try(FileInputStream in = new FileInputStream(new File(rootDir, name.replaceAll("\\.", "/")+".class"))) {
            b = in.readAllBytes();
        } catch (Exception e) {
            throw new ClassNotFoundException("无法获取["+name+"]的class文件", e);
        }
        synchronized (getClassLoadingLock(name)) {
            Class<?> c = findLoadedClass(name);
            if(c==null) {
                c = defineClass(name, b, 0, b.length);
            }
            resolveClass(c);
            return c;
        }
    }
}
