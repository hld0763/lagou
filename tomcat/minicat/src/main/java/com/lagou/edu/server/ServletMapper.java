package com.lagou.edu.server;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *简单封装servlet映射
 */
public class ServletMapper {
    private Map<String, Host> hostMap = new HashMap<>();

    /**
     * 解析目录，生成映射
     * @param name 域名
     * @param appBase 目录
     */
    public void addMapper(String name, String appBase) {
        Host host = new Host();
        File webapps = new File(appBase);
        //遍历webapps，解析所有应用
        for (File webapp : webapps.listFiles(File::isDirectory)) {
            Context context = new Context(webapp);
            context.loadServlet(new File(webapp, "web.xml"));
            host.contextMap.put(webapp.getName(), context);
        }
        hostMap.put(name, host);
    }

    /**
     * 根据域名和地址获取对应的servlet
     * @param host
     * @param context
     * @param wrapper
     * @return
     */
    public HttpServlet getServlet(String host, String context, String wrapper) {
        Host hostObj = hostMap.get(host);
        if(hostObj!=null) {
            Context contextObj = hostObj.contextMap.get(context);
            if(contextObj!=null) {
                return contextObj.wrapperMap.get(wrapper);
            }
        }
        return null;
    }

    /**
     * 根据域名和地址获取对应的静态资源文件
     * @param host
     * @param context
     * @param wrapper
     * @return
     */
    public File getResource(String host, String context, String wrapper) {
        Host hostObj = hostMap.get(host);
        if(hostObj!=null) {
            Context contextObj = hostObj.contextMap.get(context);
            if(contextObj!=null) {
                File file = new File(contextObj.rootDir, wrapper);
                if(file.isFile()) return file;
            }
        }
        return null;
    }

    private class Host {
        private Map<String, Context> contextMap = new HashMap<>();
    }

    private class Context {
        private File rootDir;
        private Map<String, HttpServlet> wrapperMap = new HashMap<>();

        public Context(File rootDir) {
            this.rootDir = rootDir;
        }

        /**
         * 加载解析web.xml，初始化servlet
         */
        private void loadServlet(File web) {
            if(!web.isFile()) return;
            SAXReader saxReader = new SAXReader();
            try {
                ServletClassLoader servletClassLoader = new ServletClassLoader(this.rootDir);
                Document document = saxReader.read(web);
                Element rootElement = document.getRootElement();
                List<Element> selectNodes = rootElement.selectNodes("//servlet");
                for (Element element : selectNodes) {
                    //<servlet-name>lagou</servlet-name>
                    String servletName = element.selectSingleNode("servlet-name").getStringValue();
                    //<servlet-class>com.lagou.edu.server.LagouServlet</servlet-class>
                    String servletClass = element.selectSingleNode("servlet-class").getStringValue();
                    //<url-pattern>/lagou</url-pattern>
                    String urlPattern = rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']").selectSingleNode("url-pattern").getStringValue();

                    wrapperMap.put(urlPattern, (HttpServlet) servletClassLoader.loadServletClass(servletClass).getConstructor().newInstance());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
