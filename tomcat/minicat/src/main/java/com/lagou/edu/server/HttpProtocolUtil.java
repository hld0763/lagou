package com.lagou.edu.server;

/**
 * HTTP协议工具类，主要是提供响应头信息，这里只提供200和404的响应头
 */
public class HttpProtocolUtil {
    /**
     * 提供200的响应头
     *
     * @return
     */
    public static String getHttpHeader200(long contentLength) {
        return "HTTP/1.1 200 OK\n" +
                "Content-Type: text/html\n" +
                "Content-Length: " + contentLength + "\n" +
                "\r\n";
    }

    /**
     * 提供404的响应头
     *
     * @return
     */
    public static String getHttpHeader404(long contentLength) {
        return "HTTP/1.1 404 Not Found\n" +
                "Content-Type: text/html\n" +
                "Content-Length: " + contentLength + "\n" +
                "\r\n";
    }
}
