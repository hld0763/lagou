package com.lagou.edu.server;

import java.io.*;

/**
 * 把响应封装成Response对象
 */
public class Response {
    private OutputStream outputStream;

    public Response(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * 将指定字符串作为响应输出
     * @param content
     */
    public void output(String content) throws IOException {
        outputStream.write(content.getBytes());
    }

    /**
     * 将静态资源文件内容作为响应输出
     * @param path
     */
    public void outputHtml(String path) throws IOException {
        //获取静态资源文件
        File file = new File(StaticResourceUtil.getAbsolutePath(path));
        outputHtml(file);
    }

    /**
     * 将静态资源文件内容作为响应输出
     * @param file
     */
    public void outputHtml(File file) throws IOException {
        if(file!=null && file.isFile()) {
            //读取静态资源并输出内容
            StaticResourceUtil.outputStaticResource(new FileInputStream(file), outputStream);
        } else {
            //静态资源无效输出404
            output404();
        }
    }

    /**
     * 返回404的响应
     * @throws IOException
     */
    public void output404() throws IOException {
        String str404 = "<h1>404 not found</h1>";
        output(HttpProtocolUtil.getHttpHeader404(str404.length())+str404);
    }
}
