package com.lagou.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 登录权限验证
 */
public class LoginInterceptor implements HandlerInterceptor {

    /**
     * 访问前进行权限验证
     * @param request
     * @param response
     * @param handler
     * @return  true执行后续访问，false停止后续访问，直接返回
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        Object username = session.getAttribute("username");
        if(username == null) {
            response.sendRedirect(request.getContextPath() + "/login/toLogin");
            return false;
        }else{
            return true;
        }
    }

}
