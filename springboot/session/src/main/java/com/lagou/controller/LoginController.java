package com.lagou.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("login")
public class LoginController {

    @RequestMapping("toLogin")
    public String toLogin() {
        return "login";
    }

    @RequestMapping("loginSystem")
    public String loginSystem(String username, HttpSession session) {
        session.setAttribute("username",username);
        return "redirect:/";
    }

    @RequestMapping("logoutSystem")
    public String logoutSystem(HttpSession session) {
        session.removeAttribute("username");
        return "redirect:/";
    }
}
