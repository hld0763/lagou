package com.lagou.springboot.simple;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;
import java.io.File;

public class SpringApplication {
    public static void run() {
        try {
            Tomcat tomcat = new Tomcat();
            tomcat.addWebapp("",new File("tomcat.8080/webapps").getAbsolutePath());
            tomcat.start();
            tomcat.getServer().await();
        } catch (LifecycleException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }
}
